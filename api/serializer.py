from rest_framework import serializers
from amsapp.models import *


#Artis_serializer
class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = "__all__"


#Artis_POST_serializer
class ArtistPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = ['name','dob','gender','first_release_year','no_of_albums_released']


#Artis_POST_serializer
class MusicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Music
        fields = ['id','artist_id','title','album_name','genre','created_at','updated_at']
    

#Artis_POST_serializer
class MusicPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Music
        fields = ['artist_id','title','album_name','genre']


#Login serializer
class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(allow_null=False)
    password = serializers.CharField(allow_null=False)


#User serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','first_name','last_name','email','phone','dob','gender','address']


#User data post
class UserPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name','last_name','email','password','phone','dob','gender','address']


#User serializer
class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name','last_name','phone','dob','gender','address']


#Artis_CSV_EXPORT_serializer
class ArtistExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = ['name','dob','gender','first_release_year','no_of_albums_released']

#Artis_CSV_Import_serializer
class CsvSerializer(serializers.ModelSerializer):
    class Meta:
        model = Csv
        fields = ['file_name']


class CsvFileUploadSerializer(serializers.ModelSerializer):
        files = serializers.FileField()
        class Meta:
            model = Csv
            fields = ['file_name']

