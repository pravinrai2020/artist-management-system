from django.shortcuts import render
from amsapp.models import *
from api.serializer import *
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework import status
from django.db import connection
from datetime import datetime
from .mypaginations import CustomPagination
from .utils import *
from django.contrib.auth import authenticate
import csv
from django.http import HttpResponse


class LoginAPIView(APIView):
    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data.get("email")
            password = serializer.validated_data.get("password")
            user = authenticate(username=email, password=password)
            try:
                if user.is_active == True:
                    resp = get_tokens_for_user(user)
                else:
                    resp = {
                        "message": "Pending or Suspended account.."
                    }
            except Exception as e:
                print(e)
                resp = {
                    "message": "Invalid credentials of the Customer.."
                }
            print(resp)
        else:
            resp = {
                "message": "Missing information"
            }
        return Response(resp)


class GetAccessAPIView(APIView):
    def get(self, request,id):
        user = User.objects.get(id=id)
        tkn = get_tokens_for_user(user)
        resp = {
                "my_token": tkn['access']
            }
        return Response(resp)


class ArtistListAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    pagination_class = CustomPagination()
    def get(self,request):
        artists = Artist.objects.all()

        # artists = Artist.objects.raw("SELECT * FROM amsapp_artist")

        # for pagination
        page = self.pagination_class.paginate_queryset(queryset=artists, request=request)
        if page is not None:
            serializer = ArtistSerializer(page, many=True)
            resp = {
                "status": "success",
                "data": serializer.data
            }
            return self.pagination_class.get_paginated_response(resp)
        
        serializer = ArtistSerializer(artists, many=True)
        my_serialised_data = serializer.data
        for item in my_serialised_data:
             if 'created_at' in item and item['created_at']:
                datetime_obj = datetime.strptime(item['created_at'], "%Y-%m-%dT%H:%M:%S.%f%z")
                item['created_at'] = datetime_obj.strftime("%Y-%m-%d %H:%M:%S")

             if 'updated_at' in item and item['updated_at']:
                datetime_obj = datetime.strptime(item['updated_at'], "%Y-%m-%dT%H:%M:%S.%f%z")
                item['updated_at'] = datetime_obj.strftime("%Y-%m-%d %H:%M:%S")    

             if 'dob' in item and item['dob']:
                datetime_obj = datetime.strptime(item['dob'], "%Y-%m-%dT%H:%M:%S%f%z")
                item['dob'] = datetime_obj.strftime("%Y-%m-%d %H:%M:%S")

             if 'gender' in item and item['gender']:
                if item['gender']=='f':
                    gender ='Female' 
                elif item['gender']=='m':
                    gender ='Male' 
                elif item['gender']=='o':
                    gender='Others'

                item['gender'] = gender

        resp = {
            "status": "success",
            "data": my_serialised_data
            # "data": posts
        }
        return Response(resp)


# POST View
class ArtistPostAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    def post(self, request):
        serializer = ArtistPostSerializer(data=request.POST)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)


class ArtistUpdateAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    def get(self,request, id):
        try:
            artist = Artist.objects.get(id=id)
            serializer=ArtistPostSerializer(artist)
            return Response(serializer.data)
        
        except Exception as e:
            print(e,"Errorrrrr####")

    def put(self, request, id):
        artist = Artist.objects.get(id=id)
        
        serializer = ArtistPostSerializer(artist,data=request.POST)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
    

class ArtistDeleteAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    def get(self,request, id):
        try:
            artist = Artist.objects.get(id=id)
            serializer=ArtistSerializer(artist)
            return Response(serializer.data)
        
        except Exception as e:
            print(e,"Errorrrrr####")

    def delete(self,request,id):
        artist = Artist.objects.get(id=id)
        artist.delete() 
        return Response(status=status.HTTP_204_NO_CONTENT)
 

#Music list
class MusicListAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    pagination_class = CustomPagination()
    def get(self,request,id):
        artist = Artist.objects.get(id=id)

        musics = Music.objects.filter(artist_id = artist)
                   
        music = Music.objects.raw("SELECT * FROM amsapp_music WHERE artist_id =%s",[artist.id])

        # for pagination
        page = self.pagination_class.paginate_queryset(queryset=musics, request=request)
        if page is not None:
            serializer = MusicSerializer(page, many=True)
            resp = {
                "status": "success",
                "singer": artist.name,
                "data": serializer.data
            }
            if not musics:
                resp = {
                "status": "Music not found",
                "singer": artist.name,
                }
            return self.pagination_class.get_paginated_response(resp)
        

# Music Post View
class MusicPostAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    def get(self, request, *args, **kwargs):
        music_id = self.kwargs.get('id')

        artist = Artist.objects.get(id=music_id)
        serializer = MusicPostSerializer(artist)
        return Response({'data': serializer.data})
    
    
    def post(self, request):
        serializer = MusicPostSerializer(data=request.POST)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)


#Music Update 
class MusicUpdateAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    def get(self,request, id):
        try:
            music = Music.objects.get(id=id)
            serializer=MusicPostSerializer(music)
            return Response(serializer.data)
        
        except Exception as e:
            print(e,"Errorrrrr####")

    def put(self, request, id):
        music = Music.objects.get(id=id)
        serializer = MusicPostSerializer(music,data=request.POST)
        if serializer.is_valid():
            serializer = serializer.save()
            serialized = MusicPostSerializer(serializer)
            return Response(serialized.data,status=status.HTTP_201_CREATED)


#music delete
class MusicDeleteAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    def get(self,request, id):
        try:
            music = Music.objects.get(id=id)
            serializer=MusicSerializer(music)
            return Response(serializer.data)
        
        except Exception as e:
            print(e,"Errorrrrr####")

    
    def delete(self,request,id):
        music = Music.objects.get(id=id)
        music.delete() 
        return Response(status=status.HTTP_204_NO_CONTENT)
 

 
#User list
class UserListAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    permission_classes = [UserOnlyPermission]

    pagination_class = CustomPagination()
    def get(self,request):
        # users = User.objects.all()

        users = User.objects.raw("SELECT * FROM amsapp_user")

        # for pagination
        page = self.pagination_class.paginate_queryset(queryset=users, request=request)
        if page is not None:
            serializer = UserSerializer(page, many=True)
            resp = {
                "status": "success",
                "data": serializer.data
            }
            if not users:
                resp = {
                "status": "User not found",
                }
            return self.pagination_class.get_paginated_response(resp)
        

# User Post View
class UserPostAPIView(APIView):

    def post(self, request):
        serializer = UserPostSerializer(data=request.POST)
        if not serializer.is_valid():
            # serializer.save()
            return Response(serializer.data,status=status.HTTP_400_BAD_REQUEST)
            # return render(request, self.template_name,{'form':form,'smg_error':'Invalid form'})

        try:
            first_name = serializer.validated_data.get('first_name')
            last_name = serializer.validated_data.get('last_name')
            email = serializer.validated_data.get('email')
            password = serializer.validated_data.get('password')
            phone = serializer.validated_data.get('phone')
            dob = serializer.validated_data.get('dob')
            gender = serializer.validated_data.get('gender')
            address = serializer.validated_data.get('address')
            user = User.objects.create_user(first_name=first_name,last_name=last_name,email=email,password=password,phone=phone,dob=dob,gender=gender,address=address)
            return Response(serializer.data,status=status.HTTP_201_CREATED)

        except Exception as e:
            print(e,"Errorrrrrrrrrr")
            return Response(serializer.data,status=status.HTTP_400_BAD_REQUEST)

            # return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})

#User Update 
class UserUpdateAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    def get(self,request, id):
        try:
            user = User.objects.get(id=id)
            serializer=UserUpdateSerializer(user)
            return Response(serializer.data)
        
        except Exception as e:
            print(e,"Errorrrrr####")

    def put(self, request, id):
        user = User.objects.get(id=id)
        serializer = UserUpdateSerializer(user,data=request.POST)
        if serializer.is_valid():
            serializer = serializer.save()
            serialized = UserUpdateSerializer(serializer)
            return Response(serialized.data,status=status.HTTP_201_CREATED)


#user delete
class UserDeleteAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    def get(self,request, id):
        try:
            user = User.objects.get(id=id)
            serializer=UserSerializer(user)
            return Response(serializer.data)
        
        except Exception as e:
            print(e,"Errorrrrr####")

    
    def delete(self,request,id):
        user = User.objects.get(id=id)
        user.delete() 
        return Response(status=status.HTTP_204_NO_CONTENT)
 
#CSV EXPORT
class CSVExportArtistAPIView(APIView):
    permission_classes = [UserOnlyPermission]


    def get(self,request):
        artists = Artist.objects.all()
        serializer=ArtistExportSerializer(artists, many=True)

        return Response(serializer.data)
        


#CSV IMPORT
class CSVImportArtistAPIView(APIView):
    permission_classes = [UserOnlyPermission]

    def post(self, request):
        file = request.FILES.get('file')
        serializer = CsvFileUploadSerializer(data=file)

        if serializer.is_valid():
            serializer.save(file)


        # file_path = '/csvs/file.txt'
        # with open(file_path, 'wb') as destination:
        #     for chunk in file.chunks():
        #         destination.write(chunk)

        # Return a response indicating the file was received successfully
        # return Response({'message': 'File received successfully'})
'''
        print("POST API##################################")
        file = request.FILES.get('file_name')
        data = request.POST.get('file_name')  
        mfile = request.data.get('file_name')  
        print(file,"################################file")
        print(data,"################################data22")
        print(mfile,"################################datafile22")

        serializer = CsvSerializer(data = request.FILES)
        if serializer.is_valid():
            try:
        
                print("IM REACH #######################",serializer)
                serializer.save()
            except Exception as e:
                print("#################exceiption",e)
            return Response(status=status.HTTP_201_CREATED)

        file = request.FILES.get('file_name')
        data = request.data.get('data')  # Assuming 'data' is the key for your additional form data
        
        if file and data:
            # Create a new instance of your model and populate its fields
            model_instance = YourModel()
            model_instance.data_field = data

            # Save the file to a specific location
            file_path = '/path/to/save/file'  # Replace with your desired file path
            with open(file_path, 'wb') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)
            
            # Set the file field of the model instance to the file path
            model_instance.file_field = file_path
            
            # Save the model instance to the database
            model_instance.save()
            
            # Return a success response
            return Response({'message': 'File and data saved successfully'}, status=200)
        else:
            # Return an error response if either file or data is missing
            return Response({'message': 'File or data missing'}, status=400)
            '''