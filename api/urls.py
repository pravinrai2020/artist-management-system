from django.urls import path
from .views import *

urlpatterns =[
    path("login/", LoginAPIView.as_view(), name="loginapi"),
    path('get-access/<int:id>/',GetAccessAPIView.as_view(), name="get_access"),

    path('artist/post/',ArtistPostAPIView.as_view(),name="artist_post"),
    path('artist/<int:id>/update/',ArtistUpdateAPIView.as_view(),name="artist_update"),
    path('artist/<int:id>/delete/',ArtistDeleteAPIView.as_view(),name="artist_delete"),
    path('artists/',ArtistListAPIView.as_view(),name="artist_list"),

    path('music/post/',MusicPostAPIView.as_view(),name="music_post"),
    path('music/<int:id>/update/',MusicUpdateAPIView.as_view(),name="music_update"),
    path('music/<int:id>/delete/',MusicDeleteAPIView.as_view(),name="music_delete"),
    path('musics/<int:id>/',MusicListAPIView.as_view(),name="music_list"),

    path('user/post/',UserPostAPIView.as_view(),name="user_post"),
    path('user/<int:id>/update/',UserUpdateAPIView.as_view(),name="user_update"),
    path('user/<int:id>/delete/',UserDeleteAPIView.as_view(),name="user_delete"),
    path('users/',UserListAPIView.as_view(),name="user_list"),

    path('artist-exp/',CSVExportArtistAPIView.as_view(), name="artist_export"),
    path('artist-imp/',CSVImportArtistAPIView.as_view(), name="artist_import"),

]
