from django.db import models
from django.contrib.auth.models import AbstractUser
from .manager import UserManager

GENDER =(
    ('m','Male'),
    ('f','Female'),
    ('o','Other'),
)

GENRE =(
    ('RNB','RNB'),
    ('Country','Country'),
    ('Classic','Classic'),
    ('Rock','Rock'),
    ('Jazz','Jazz'),
)
#Custome_user
class User(AbstractUser):
    first_name=models.CharField(max_length=255)
    last_name=models.CharField(max_length=255)
    email=models.EmailField(max_length=255,unique=True)
    password=models.CharField(max_length=500)
    phone=models.CharField(max_length=20)
    dob = models.DateTimeField(null=True, blank=True)
    gender = models.CharField(max_length=20, choices=GENDER,null=True, blank=True)
    address = models.CharField(max_length=255,null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True,auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=False,auto_now=True)
    username = None

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS =[]
    objects = UserManager()

    class Meta:
        ordering =['-id']

    def __str__(self):
        return self.email

#Artist
class Artist(models.Model):
    name = models.CharField(max_length=255)
    dob = models.DateTimeField(null=True, blank=True)
    gender = models.CharField(max_length=20, choices=GENDER,null=True, blank=True)
    first_release_year = models.DateField(null=True, blank=True)
    no_of_albums_released = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True,auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=False,auto_now=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.name
    

#Music
class Music(models.Model):
    artist_id = models.ForeignKey(Artist,on_delete=models.SET_NULL,null=True,blank=True)
    title = models.CharField(max_length=255)
    album_name = models.CharField(max_length=255,null=True, blank=True)
    genre = models.CharField(max_length=20, choices=GENRE,null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True,auto_now=False)
    updated_at = models.DateTimeField(auto_now_add=False,auto_now=True)

    def __str__(self):
        return self.title


class Csv(models.Model):
    file_name = models.FileField(upload_to='csvs/')
    created_at = models.DateTimeField(auto_now_add=True,auto_now=False)
    is_activated = models.BooleanField(default=False)

    def __str__(self):
        return f'File id: {self.id}'
