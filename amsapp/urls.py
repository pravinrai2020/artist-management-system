from django.urls import path,include
from .views import *

app_name = 'amsapp'

urlpatterns = [
    path('',ArtistListView.as_view(),name="artist_list"),
    path('artists/add/',ArtistAddView.as_view(),name="artist_add"),
    path('artist/<int:id>/update/',ArtistUpdateView.as_view(),name="artist_update"),
    path('artist/<int:id>/delete/',ArtistDeleteView.as_view(),name="artist_delete"),

    path('musics/<int:id>/',MusicListView.as_view(),name="music_list"),
    path('musics/add/<int:id>/',MusicAddView.as_view(),name="music_add"),
    path('music/<int:id>/update/',MusicUpdateView.as_view(),name="music_update"),
    path('music/<int:id>/delete/',MusicDeleteView.as_view(),name="music_delete"),   

    path('users/',UserListView.as_view(),name="user_list"),
    path('users/add/',UserAddView.as_view(),name="user_add"),
    path('user/<int:id>/update/',UserUpdateView.as_view(),name="user_update"),
    path('user/<int:id>/delete/',UserDeleteView.as_view(),name="user_delete"),   

    path('login/',ArtistLoginView.as_view(),name="login"),
    path('logout/',LogoutView.as_view(),name="logout"),

    path('export/',ArtitsFileExportView.as_view(),name="export"),
    path('import/',ArtitsFileImportView.as_view(),name="import"),

    
    
]

