from django import forms
from .models import *


class LoginForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(
        attrs={"class": "form-control"}))
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control"}))



class ArtistForm(forms.ModelForm):
    class Meta:
        model = Artist
        fields = ['name','dob','gender','first_release_year','no_of_albums_released']
        widgets ={
            'name':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Artist name"
            }),
            'dob':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Artist date of birth"
            }),
            'gender':forms.Select(attrs={
                'class':'form-control',
                'placeholder':"Artist date of birth"
            }),
            'first_release_year':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Artist first album felease date"
            }),
            'no_of_albums_released':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Number of release album"

            }),
        }

class MusicForm(forms.ModelForm):
    class Meta:
        model = Music
        fields = ['title','album_name','genre']
        widgets ={
            'title':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Music title"
            }),
            'album_name':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Album name"
            }),
            'genre':forms.Select(attrs={
                'class':'form-control',
                'placeholder':"Select Genre"
            }),

        }


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email','password','first_name','last_name','phone','dob','gender','address']
        widgets ={
            'email':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Email"
            }),            
            'password':forms.PasswordInput(attrs={
                'class':'form-control',
                'placeholder':"Password"
            }),
            'first_name':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"First name"
            }),
            'last_name':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Last name"
            }),
            'phone':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Phone number"
            }),
            'dob':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Date of birth"
            }),
            'gender':forms.Select(attrs={
                'class':'form-control',
                'placeholder':"Artist date of birth"
            }),
            'address':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Address"
            }),
        }
    def clean_signup_email(self):
        email = self.cleaned_data.get("email")
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError(
                "This email is already used, please use different email.")
        return email



class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name','last_name','phone','dob','gender','address']
        widgets ={
            'first_name':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"First name"
            }),
            'last_name':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Last name"
            }),
            'phone':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Phone number"
            }),
            'dob':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Date of birth"
            }),
            'gender':forms.Select(attrs={
                'class':'form-control',
                'placeholder':"Artist date of birth"
            }),
            'address':forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':"Address"
            }),
        }


class CSVUploadForm(forms.ModelForm):
    class Meta:
        model = Csv
        fields = ('file_name',)
        widgets ={
            'file_name':forms.ClearableFileInput(attrs={
                'class':'form-control',
                'placeholder':"First name"
            }),
            }