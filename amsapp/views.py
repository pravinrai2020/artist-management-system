from typing import Any, Dict
from django.shortcuts import render,redirect
from .models import *
from django.views import View
from django.views.generic import TemplateView

from django.views.generic import View
from django.http import JsonResponse
import requests
from datetime import datetime
from .forms import *
from datetime import datetime as dt
from django.contrib.auth import login
from django.urls import reverse
import csv
from django.http import HttpResponse


#loggin mixin
class LoginRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        access_key = request.session.get('access_key') if 'access_key' in request.session else None
        self.headers = {'Authorization': f'Bearer {access_key}'}
        user_id = self.request.session.get("user_id")

        if user_id is None:
            return redirect(reverse('amsapp:login'))
        else:

            url = f'http://127.0.0.1:8000/api/get-access/{user_id}/' 
            response = requests.get(url, headers=self.headers)
            data = response.json()

            if not response and 'code' in data and  data['code'] =='token_not_valid':
                del self.request.session["user_id"]
                del self.request.session["access_key"]
                return redirect(reverse('amsapp:login'))

        return super().dispatch(request, *args, **kwargs)
    

#logout
class LogoutView(LoginRequiredMixin, View):
    def get(self, request):
        if "user_id" in self.request.session.keys():
            del self.request.session["user_id"]
            del self.request.session["access_key"]
        return redirect("amsapp:login")


#User Login
class ArtistLoginView(TemplateView):
    template_name = 'login.html'
    
    def get(self, request,*args,**kwargs):
        access_key = request.session.get('access_key') if 'access_key' in request.session else None
        self.headers = {'Authorization': f'Bearer {access_key}'}
        user_id = self.request.session.get("user_id")

        if user_id is not None:
            url = f'http://127.0.0.1:8000/api/get-access/{user_id}/' 
            response = requests.get(url, headers=self.headers)
            data = response.json()

            if not response and 'code' in data and  data['code'] =='token_not_valid':
                pass
            else:
                return redirect("amsapp:artist_list")


        forms = LoginForm

        return render(request,self.template_name,{'forms':forms})
    
    def post(self, request,*args,**kwargs):
        url = 'http://127.0.0.1:8000/api/login/' 
        form=LoginForm(request.POST)
        if not form.is_valid():
            print("Not valid")
            return render(request,self.template_name,{'forms':form})
        data ={
            'email':form.cleaned_data.get('email'),
            'password':form.cleaned_data.get('password'),
        }
        response = requests.post(url, data=data)
        data = response.json()
        access_key= data['access'] 
        email= data['email'] 
        user_id= data['u_id'] 
        user_name= data['full_name'] 

        if email is not None:
            request.session['access_key'] = access_key
            request.session['user_email'] = email
            request.session['user_id'] = user_id
            request.session['user_name'] = user_name
            return redirect("amsapp:artist_list")


        if response.status_code == 201:
            print('Data posted successfully')
        else:
            print('Failed to post data')
            return render(request,self.template_name,{'forms':form})
        
        return redirect("amsapp:artist_list")

   
class ArtistListView(LoginRequiredMixin, View):
    template_name = "artist_list.html"

    def get(self, request, *args, **kwargs):
        page_num = self.request.GET.get('page', 1)

        records = 10
        url = f'http://127.0.0.1:8000/api/artists/?page={page_num}&records={records}'
        
        response = requests.get(url,headers=self.headers)
        if response:
            data = response.json()

            count = int(data['count'])
            results = data['results']
            
            # Pagination data 
            total_page = count//records if count%records ==0 else (count//records)+1 
            pages = [i for i in range(1,total_page+1)]

            if page_num == 'last':
                previous = pages[-2]
                next_page = None
                current_page = int(pages[-1])
            else:
                previous = int(page_num)-1
                next_page = int(page_num)+1
                current_page = int(page_num)

            if page_num == 1:
                previous = None
            
            if current_page == int(pages[-1]):
                next_page = None

            records_list = results['data']
            status = results['status']

            for item in records_list:
                if 'created_at' in item and item['created_at']:
                    item['created_at'] = item['created_at'][:10]
            #         date_part = datetime.strptime(item['created_at'], "%Y-%m-%d %H:%M:%S").date()
            #         item['created_at'] = date_part.isoformat()
                if 'updated_at' in item and item['updated_at']:
                    # date_part = datetime.strptime(item['updated_at'], "%Y-%m-%d %H:%M:%S").date()
                    item['updated_at'] = item['updated_at'][:10] 
                if 'dob' in item and item['dob']:
                    # date_part = datetime.strptime(item['dob'], "%Y-%m-%d %H:%M:%S").date()
                    item['dob'] = item['dob'][:10]
            return render(request,self.template_name,{'artists':records_list,'previous':previous,'next_page':next_page,'pages':pages,'current_page':current_page})
        else:
            return render(request,self.template_name,{'artists':""})

class ArtistAddView(LoginRequiredMixin,View):
    template_name ="artist_add.html"

    def get(self, request,*args,**kwargs):
        forms = ArtistForm

        return render(request,self.template_name,{'forms':forms})
    
    def post(self, request,*args,**kwargs):
        url = 'http://127.0.0.1:8000/api/artist/post/' 
        form=ArtistForm(request.POST)
        if not form.is_valid():
            print("Not valid")
            return render(request,self.template_name,{'forms':form})
        data ={
            'name':form.cleaned_data.get('name'),
            'dob':form.cleaned_data.get('dob'),
            'gender':form.cleaned_data.get('gender'),
            'first_release_year':form.cleaned_data.get('first_release_year'),
            'no_of_albums_released':form.cleaned_data.get('no_of_albums_released')
        }
        response = requests.post(url, data=data, headers=self.headers)

        if response.status_code == 201:
            print('Data posted successfully')
        else:
            print('Failed to post data')
            return render(request,self.template_name,{'forms':form})
        
        return redirect("amsapp:artist_list")

class ArtistUpdateView(LoginRequiredMixin,View):
    template_name ="artist_add.html"

    def get(self, request,*args,**kwargs):
        a_id =self.kwargs.get('id')
        url = f'http://127.0.0.1:8000/api/artist/{a_id}/update/'

        response = requests.get(url, headers=self.headers)
        if response:
            data = response.json()
        forms = ArtistForm(data)

        return render(request,self.template_name,{'forms':forms})
    
    def post(self, request,*args,**kwargs):
        a_id =self.kwargs.get('id')
        url = f'http://127.0.0.1:8000/api/artist/{a_id}/update/' 
        form=ArtistForm(data=request.POST)

        if not form.is_valid():
            return render(request,self.template_name,{'forms':form})
        data ={
            'id':a_id,
            'name':form.cleaned_data.get('name'),
            'dob':form.cleaned_data.get('dob'),
            'gender':form.cleaned_data.get('gender'),
            'first_release_year':form.cleaned_data.get('first_release_year'),
            'no_of_albums_released':form.cleaned_data.get('no_of_albums_released')
        }
        response = requests.put(url, data=data, headers=self.headers)

        if response.status_code == 201:
            print('Data posted successfully')
            return redirect("amsapp:artist_list")
        else:
            print('Failed to post data')
            return render(request,self.template_name,{'forms':form,'msg_error':"Oops! Something went wrong."})

        
class ArtistDeleteView(LoginRequiredMixin, View):
    template_name ="artist_delete.html"

    def get(self, request, *args, **kwargs):
        a_id = self.kwargs.get('id')
        # artist = Artist.objects.get(id=a_id)

        url = f'http://127.0.0.1:8000/api/artist/{a_id}/delete/'
        response = requests.get(url, headers=self.headers)
        artist = response.json()
        return render(request,self.template_name,{'obj':artist}) 


    def post(self, request,*args,**kwargs):
        a_id =self.kwargs.get('id')
        url = f'http://127.0.0.1:8000/api/artist/{a_id}/delete/' 
 
        data ={
            'id':a_id,
        }
        response = requests.delete(url, data=data, headers=self.headers)

        if response.status_code == 201:
            print('Data posted successfully')
        else:
            print('Failed to post data')
        
        return redirect("amsapp:artist_list")

 
class MusicListView(LoginRequiredMixin, View):
    template_name = "music/music_list.html"

    def get(self, request, *args, **kwargs):
        artist_id = self.kwargs.get('id')

        page_num = self.request.GET.get('page', 1)
        records = 10
        url = f'http://127.0.0.1:8000/api/musics/{artist_id}/?page={page_num}&records={records}'
        response = requests.get(url, headers=self.headers)
        if response :
  
                data = response.json()
                count = int(data['count'])
                results = data['results']
                
                    

                # Pagination data 
                total_page = count//records if count%records ==0 else (count//records)+1 
                pages = [i for i in range(1,total_page+1)]
                singer = results['singer']

                try:
                    if page_num == 'last':
                        previous = pages[-2]
                        next_page = None
                        current_page = int(pages[-1])
                    else:
                        previous = int(page_num)-1
                        next_page = int(page_num)+1
                        current_page = int(page_num)

                    if page_num == 1:
                        previous = None
                
                    if current_page == int(pages[-1]):
                        next_page = None
                    
                except Exception as e:
                    print("Errorrrrrrrrr",e)
                    return render(request,self.template_name,{'musics':"",'singer':singer,'artist_id':artist_id})

                records_list = results['data']
                status = results['status']

                for item in records_list:
                    if 'created_at' in item and item['created_at']:
                        item['created_at'] = item['created_at'][:10]
                    if 'updated_at' in item and item['updated_at']:
                        item['updated_at'] = item['updated_at'][:10] 

                return render(request,self.template_name,{'musics':records_list,'previous':previous,'next_page':next_page,'pages':pages,'current_page':current_page,'singer':singer,'artist_id':artist_id})
            
            
        else:
            return render(request,self.template_name,{'musics':""})

class MusicAddView(LoginRequiredMixin, View):
    template_name ="music/music_add.html"

    def get(self, request,*args,**kwargs):
        artist_id = int(self.kwargs.get('id'))
        
        forms = MusicForm

        return render(request,self.template_name,{'forms':forms,'artist_id':artist_id})
    
    def post(self, request,*args,**kwargs):
        artist_id = int(self.kwargs.get('id'))
        
        url = 'http://127.0.0.1:8000/api/music/post/' 
        form=MusicForm(request.POST)
        if not form.is_valid():
            print("Not valid")
            return render(request,self.template_name,{'forms':form,'artist_id':artist_id})
        data ={
            'artist_id':artist_id,
            'title':form.cleaned_data.get('title'),
            'album_name':form.cleaned_data.get('album_name'),
            'genre':form.cleaned_data.get('genre'),
          }
        response = requests.post(url, data=data, headers=self.headers)

        if response.status_code == 201:
            print('Data posted successfully')
        else:
            print('Failed to post data')
            return render(request,self.template_name,{'forms':form,'artist_id':artist_id})
        
        return redirect("amsapp:music_list",artist_id)

class MusicUpdateView(LoginRequiredMixin, View):
    template_name ="music/music_add.html"

    def get(self, request,*args,**kwargs):
        a_id =self.kwargs.get('id')
        url = f'http://127.0.0.1:8000/api/music/{a_id}/update/'

        response = requests.get(url, headers=self.headers)

        if response:
            data = response.json()
        forms = MusicForm(data)

        return render(request,self.template_name,{'forms':forms})
    
    def post(self, request,*args,**kwargs):
        music_id =self.kwargs.get('id')
        url = f'http://127.0.0.1:8000/api/music/{music_id}/update/' 

        form=MusicForm(data=request.POST)

        if not form.is_valid():
            return render(request,self.template_name,{'forms':form})
        data ={
            'title':form.cleaned_data.get('title'),
            'album_name':form.cleaned_data.get('album_name'),
            'genre':form.cleaned_data.get('genre'),
          }
        response = requests.put(url, data=data, headers=self.headers)
        music_resp = response.json()
        if response.status_code == 201:
            print('Data posted successfully')
            return redirect("amsapp:music_list",music_resp['artist_id'])
        else:
            print('Failed to post data')
            if music_resp['artist_id']:
                return redirect("amsapp:music_list",music_resp['artist_id'])
            else:
                return render(request,self.template_name,{'forms':form,"msg_error":"Something went wrong! please try again."})


class MusicDeleteView(LoginRequiredMixin, View):
    template_name ="music/music_delete.html"

    def get(self, request, *args, **kwargs):
        a_id = self.kwargs.get('id')

        url = f'http://127.0.0.1:8000/api/music/{a_id}/delete/'
        response = requests.get(url,headers=self.headers)
        music = response.json()

        return render(request,self.template_name,{'obj':music}) 


    def post(self, request,*args,**kwargs):
        a_id =self.kwargs.get('id')
        artist_id = self.request.POST.get('artist_id')
        url = f'http://127.0.0.1:8000/api/music/{a_id}/delete/' 
 
        data ={
            'id':a_id,
        }
        response = requests.delete(url, data=data, headers=self.headers)

        if response.status_code == 201:
            print('Data posted successfully')
        else:
            print('Failed to post data')
        
        return redirect("amsapp:music_list",artist_id)



class UserListView(LoginRequiredMixin, View):
    template_name = "user/user_list.html"

    def get(self, request, *args, **kwargs):
        page_num = self.request.GET.get('page', 1)
        records = 10
        url = f'http://127.0.0.1:8000/api/users/?page={page_num}&records={records}'
        response = requests.get(url, headers=self.headers)
        if response:
            data = response.json()
            count = int(data['count'])
            results = data['results']
            
                

            # Pagination data 
            total_page = count//records if count%records ==0 else (count//records)+1 
            pages = [i for i in range(1,total_page+1)]

            if page_num == 'last':
                previous = pages[-2]
                next_page = None
                current_page = int(pages[-1])
            else:
                previous = int(page_num)-1
                next_page = int(page_num)+1
                current_page = int(page_num)

            if page_num == 1:
                previous = None
            
            if current_page == int(pages[-1]):
                next_page = None




            records_list = results['data']
            status = results['status']

            for item in records_list:
                if 'created_at' in item and item['created_at']:
                    item['created_at'] = item['created_at'][:10]
                if 'updated_at' in item and item['updated_at']:
                    item['updated_at'] = item['updated_at'][:10] 
                if 'dob' in item and item['dob']:
                    item['dob'] = item['dob'][:10]

            return render(request,self.template_name,{'users':records_list,'previous':previous,'next_page':next_page,'pages':pages,'current_page':current_page})
        else:
            return render(request,self.template_name,{'users':""})

class UserAddView(View):
    template_name ="user/user_registration.html"

    def get(self, request,*args,**kwargs):
        forms = UserForm

        return render(request,self.template_name,{'forms':forms})
    
    def post(self, request,*args,**kwargs):
        url = 'http://127.0.0.1:8000/api/user/post/' 
        form=UserForm(request.POST)
        if not form.is_valid():
            print("Not valid")
            return render(request,self.template_name,{'forms':form})
        data ={
            'first_name':form.cleaned_data.get('first_name'),
            'last_name':form.cleaned_data.get('last_name'),
            'email':form.cleaned_data.get('email'),
            'password':form.cleaned_data.get('password'),
            'phone':form.cleaned_data.get('phone'),
            'dob':form.cleaned_data.get('dob'),
            'gender':form.cleaned_data.get('gender'),
            'address':form.cleaned_data.get('address'),
        }
        response = requests.post(url, data=data)

        if response.status_code == 201:
            print('Data posted successfully')
        else:
            print('Failed to post data')
            return render(request,self.template_name,{'forms':form})
        
        return redirect("amsapp:user_list")

class UserUpdateView(LoginRequiredMixin, View):
    template_name ="user/user_update.html"

    def get(self, request,*args,**kwargs):

        a_id =self.kwargs.get('id')
        url = f'http://127.0.0.1:8000/api/user/{a_id}/update/'

        response = requests.get(url, headers=self.headers)
        if response:
            data = response.json()
        
        forms = UserUpdateForm(data)

        return render(request,self.template_name,{'forms':forms})
    
    def post(self, request,*args,**kwargs):
        a_id =self.kwargs.get('id')
        url = f'http://127.0.0.1:8000/api/user/{a_id}/update/' 
        form=UserUpdateForm(data=request.POST)

        if not form.is_valid():
            return render(request,self.template_name,{'forms':form})
        data ={
            'first_name':form.cleaned_data.get('first_name'),
            'last_name':form.cleaned_data.get('last_name'),
            'phone':form.cleaned_data.get('phone'),
            'dob':form.cleaned_data.get('dob'),
            'gender':form.cleaned_data.get('gender'),
            'address':form.cleaned_data.get('address'),
        }
        response = requests.put(url, data=data, headers=self.headers)

        if response.status_code == 201:
            print('Data posted successfully')
            return redirect("amsapp:user_list")
        else:
            print('Failed to post data')
            return render(request,self.template_name,{'forms':form,'msg_error':"Oops! Something went wrong."})

        
class UserDeleteView(LoginRequiredMixin, View):
    template_name ="user/user_delete.html"

    def get(self, request, *args, **kwargs):
        a_id = self.kwargs.get('id')
        # user = User.objects.get(id=a_id)

        url = f'http://127.0.0.1:8000/api/user/{a_id}/delete/'
        response = requests.get(url, headers=self.headers)
        user = response.json()
        return render(request,self.template_name,{'obj':user}) 


    def post(self, request,*args,**kwargs):
        a_id =self.kwargs.get('id')
        url = f'http://127.0.0.1:8000/api/user/{a_id}/delete/' 
 
        data ={
            'id':a_id,
        }
        response = requests.delete(url, data=data, headers=self.headers)

        if response.status_code == 201:
            print('Data posted successfully')
        else:
            print('Failed to post data')
        
        return redirect("amsapp:user_list")

 
#csv export view
class ArtitsFileExportView(LoginRequiredMixin, View):
    template_name ="user/user_delete.html"

    def get(self, request,*args,**kwargs):

        url = f'http://127.0.0.1:8000/api/artist-exp/'

        response = requests.get(url, headers=self.headers)
        if response:
            data = response.json()
        # Create the HTTP response with CSV content
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="Artists.csv"'

        # Create a CSV writer and write the data to the response
        writer = csv.writer(response)
        
        # Write header row
        writer.writerow(['Name', 'D.O.B', 'Gender','First Album','Total Album'])

        # Write data rows
        for item in data:
            writer.writerow([item['name'], item['dob'],item['gender'], item['first_release_year'],item['no_of_albums_released']])
        
        return response


#csv import view
class ArtitsFileImportView(LoginRequiredMixin, View):
    template_name ="artist_upload_csv.html"

    def get(self, request,*args,**kwargs):
        forms = CSVUploadForm

        return render(request,self.template_name,{'form':forms})
    
    def post(self,request, *args, **kwargs):
        url = f'http://127.0.0.1:8000/api/artist-imp/'

        form = CSVUploadForm(request.POST,request.FILES)
        if not form.is_valid():
            return render(request,self.template_name,{'form':form})
        
        file = request.FILES.get('file_name') 
        files = {'file': file}

        response = requests.post(url,files=files,headers=self.headers)

        if response.status_code == 201:
            print('Data posted successfully')
        else:
            print('Failed to post data')
            return render(request,self.template_name,{'forms':form})
        
        return redirect("amsapp:artist_list")
