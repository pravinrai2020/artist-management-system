from django.urls import path
from .views import *

app_name = 'artistapp'

urlpatterns =[
    path('',ArtistListView.as_view(),name="a_artist_list"),
    path('artist/add/',ArtistAddView.as_view(),name="a_artist_add"),
    path('artist/<int:id>/update/',ArtistUpdateView.as_view(),name="a_artist_update"),
    path('artist/<int:id>/delete/',ArtistDeleteView.as_view(),name="a_artist_delete"),

    path('musics/<int:id>/',MusicListView.as_view(),name="a_music_list"),
    path('music/add/<int:id>/',MusicAddView.as_view(),name="a_music_add"),
    path('music/<int:id>/update/',MusicUpdateView.as_view(),name="a_music_update"),
    path('music/<int:id>/delete/',MusicDeleteView.as_view(),name="a_music_delete"),

    path('users/',UserListView.as_view(),name="a_user_list"),
    path('user/add/',UserAddView.as_view(),name="a_user_add"),
    path('user/<int:id>/update/',UserUpdateView.as_view(),name="a_user_update"),
    path('user/<int:id>/delete/',UserDeleteView.as_view(),name="a_user_delete"),

    path('login/',ArtistLoginView.as_view(),name="a_login"),
    path('logout/',LogoutView.as_view(),name="a_logout"),

    path('csv-artist-export/',CSVExportArtistView.as_view(),name="csv_artist"),
    path('csv-artist-import/',CSVImportArtistView.as_view(),name="csv_artist_import"),
]