import csv
from datetime import datetime
from typing import Any, Dict
from django.http import HttpResponse
from django.shortcuts import render,redirect
from django.views.generic import TemplateView,View,FormView,CreateView
from amsapp.models import *
from .forms import *
from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse,reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin


class ArtistLoginRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect(reverse('artistapp:a_login')+"?next="+request.path)
        return super().dispatch(request, *args, **kwargs)


class ArtistLoginView(FormView):
    template_name = 'normal/login.html'
    form_class = LoginForm
    success_url = reverse_lazy('artistapp:a_user_list')

    def form_valid(self, form):

        email = form.cleaned_data["email"]
        password = form.cleaned_data["password"]
        user = authenticate(username=email, password=password)
        if user is not None:
            login(self.request, user)
        else:
            return render(self.request,self.template_name,{"msg_error":'Invalid username or password'})

        return super().form_valid(form)

    def get_success_url(self):
        if 'next' not in self.request.GET:
            return self.success_url
        return self.request.GET.get('next')
    
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('artistapp:a_artist_list')
        return super().dispatch(request, *args, **kwargs)


class LogoutView(ArtistLoginRequiredMixin, View):
    def get(self, request):
        logout(request)
        return redirect("artistapp:a_login")





class ArtistListView(ArtistLoginRequiredMixin,TemplateView):
    template_name = 'normal/artist_list.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        artists = Artist.objects.all()
        paginator = Paginator(artists,12)
        page_number = self.request.GET.get("page", 1)
        finalData = paginator.get_page(page_number)
        totalpage = finalData.paginator.num_pages

        context['artist_lists'] =finalData
        context['lastpage'] =totalpage
        context['current_page'] =int(page_number)
        context['totalpagelist'] =[n+1 for n in range(totalpage)]

        return context


class ArtistAddView(ArtistLoginRequiredMixin, TemplateView):
    template_name = 'normal/artist_add.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['forms'] = ArtistForm

        return context
    
    def post(self, request, *args,**kwargs):
        form = ArtistForm(request.POST)
        if not form.is_valid:
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})
        try:
            form.save()
        except Exception as e:
            print(e,"Errorrrrrrrrrr")
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})


        return redirect('artistapp:a_artist_list')


class ArtistUpdateView(ArtistLoginRequiredMixin,TemplateView):
    template_name = 'normal/artist_add.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        a_id = self.kwargs.get('id')
        artist = Artist.objects.get(id=a_id)
        context['forms'] = ArtistForm(instance=artist)

        return context
    
    def post(self, request, *args,**kwargs):
        a_id = self.kwargs.get('id')
        artist = Artist.objects.get(id=a_id)
        form = ArtistForm(request.POST,instance=artist)
        if not form.is_valid:
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})
        
        try:
            form.save()
        except Exception as e:
            print(e,"Errorrrrrrrrrr")
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})


        return redirect('artistapp:a_artist_list')


class ArtistDeleteView(ArtistLoginRequiredMixin, TemplateView):
    template_name = 'normal/artist_delete.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        a_id = self.kwargs.get('id')
        context['artist'] = Artist.objects.get(id=a_id)

        return context
    
    def post(self, request, *args,**kwargs):
        a_id = self.kwargs.get('id')
        artist = Artist.objects.get(id=a_id)
        try:
            artist.delete()
        except Exception as e:
            print(e,"Errorrrrrrrrrr")
            return render(request,self.template_name,{'artist':artist,'msg_error':"Oopa! Cannot Delete",'id':a_id})


        return redirect('artistapp:a_artist_list')



class MusicListView(ArtistLoginRequiredMixin, TemplateView):
    template_name = 'normal/music_list.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        artist_id = self.kwargs.get("id")
        artist = Artist.objects.get(id=artist_id)
        musics = Music.objects.filter(artist_id=artist)
        context['artist'] =artist

        paginator = Paginator(musics,2)
        page_number = self.request.GET.get("page",1)
        finalData = paginator.get_page(page_number)
        totalpage = finalData.paginator.num_pages

        context['music_lists'] =finalData
        context['lastpage'] =totalpage
        context['current_page'] =int(page_number)
        context['totalpagelist'] =[n+1 for n in range(totalpage)]

        return context


class MusicAddView(ArtistLoginRequiredMixin,TemplateView):
    template_name = 'normal/music_add.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        artist_id = self.kwargs.get('id')
        artist = Artist.objects.get(id=artist_id)
        context['forms'] = MusicForm
        context['artist'] = artist

        return context
    
    def post(self, request, *args,**kwargs):
        form = MusicForm(request.POST)
        artist_id = self.kwargs.get('id')
        artist = Artist.objects.get(id=artist_id)

        if not form.is_valid:
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})
        try:
            music = form.save(commit=False)
            music.artist_id = artist
            music.save()

        except Exception as e:
            print(e,"Errorrrrrrrrrr")
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})


        return redirect('artistapp:a_music_list',artist.id)


class MusicUpdateView(ArtistLoginRequiredMixin, TemplateView):
    template_name = 'normal/music_add.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        a_id = self.kwargs.get('id')
        music = Music.objects.get(id=a_id)
        artist = Artist.objects.get(id=music.artist_id.id)

        context['forms'] = MusicForm(instance=music)
        context['artist'] =artist

        return context
    
    def post(self, request, *args,**kwargs):
        a_id = self.kwargs.get('id')
        music = Music.objects.get(id=a_id)
        artist = Artist.objects.get(id=music.artist_id.id)
        form = MusicForm(request.POST,instance=music)
        if not form.is_valid:
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})
        
        try:
            form.save()
        except Exception as e:
            print(e,"Errorrrrrrrrrr")
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})


        return redirect('artistapp:a_music_list',artist.id)


class MusicDeleteView(ArtistLoginRequiredMixin, TemplateView):
    template_name = 'normal/music_delete.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        a_id = self.kwargs.get('id')
        context['music'] = Music.objects.get(id=a_id)

        return context
    
    def post(self, request, *args,**kwargs):
        a_id = self.kwargs.get('id')
        music = Music.objects.get(id=a_id)
        artist = Artist.objects.get(id=music.artist_id.id)

        try:
            music.delete()
        except Exception as e:
            print(e,"Errorrrrrrrrrr")
            return render(request,self.template_name,{'music':music,'msg_error':"Oopa! Cannot Delete",'id':a_id})


        return redirect('artistapp:a_music_list',artist.id)


class UserListView(ArtistLoginRequiredMixin, TemplateView):
    template_name = 'normal/user_list.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        users = User.objects.all()

        paginator = Paginator(users,2)
        page_number = self.request.GET.get("page", 1)
        finalData = paginator.get_page(page_number)
        totalpage = finalData.paginator.num_pages

        context['user_lists'] =finalData
        context['lastpage'] =totalpage
        context['current_page'] =int(page_number)
        context['totalpagelist'] =[n+1 for n in range(totalpage)]

        return context


class UserAddView(TemplateView):
    template_name = 'normal/user_add.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['forms'] = UserForm

        return context
    
    def post(self, request, *args,**kwargs):
        form = UserForm(request.POST)
        if not form.is_valid():
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})
        try:
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
            phone = form.cleaned_data.get('phone')
            dob = form.cleaned_data.get('dob')
            gender = form.cleaned_data.get('gender')
            address = form.cleaned_data.get('address')
            user = User.objects.create_user(first_name=first_name,last_name=last_name,email=email,password=password,phone=phone,dob=dob,gender=gender,address=address)
            user.is_staff = True
            user.save()

        except Exception as e:
            print(e,"Errorrrrrrrrrr")
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})


        return redirect('artistapp:a_login')


class UserUpdateView(ArtistLoginRequiredMixin, TemplateView):
    template_name = 'normal/user_update.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        a_id = self.kwargs.get('id')
        user = User.objects.get(id=a_id)
        context['forms'] = UserUpdateForm(instance=user)

        return context
    
    def post(self, request, *args,**kwargs):
        a_id = self.kwargs.get('id')
        user = User.objects.get(id=a_id)
        form = UserUpdateForm(request.POST,instance=user)
        if not form.is_valid:
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})
        
        try:
            form.save()
        except Exception as e:
            print(e,"Errorrrrrrrrrr")
            return render(request,self.template_name,{'forms':form,'msg_error':"Invalid Input"})


        return redirect('artistapp:a_user_list')


class UserDeleteView(ArtistLoginRequiredMixin,TemplateView):
    template_name = 'normal/user_delete.html'

    def get_context_data(self,*args, **kwargs):
        context = super().get_context_data(**kwargs)
        a_id = self.kwargs.get('id')
        context['user'] = User.objects.get(id=a_id)

        return context
    
    def post(self, request, *args,**kwargs):
        a_id = self.kwargs.get('id')
        user = User.objects.get(id=a_id)
        try:
            user.delete()
        except Exception as e:
            print(e,"Errorrrrrrrrrr")
            return render(request,self.template_name,{'user':user,'msg_error':"Oopa! Cannot Delete",'id':a_id})


        return redirect('artistapp:a_user_list')
    
class CSVImportArtistView(TemplateView):
    template_name = 'normal/artist_upload_csv.html'

    def get_context_data(self, *args,**kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CSVUploadForm()

        return context
    
    def post(self, request, *args,**kwargs):
        form = CSVUploadForm(request.POST or None, request.FILES or None)
        if not form.is_valid():
            return render(request,self.template_name,{'form':form,'msg_error':'Invalid input!'})
        form.save()
        obj = Csv.objects.filter(is_activated=False).first()
        with open(obj.file_name.path,'r') as f:
            reader = csv.reader(f)

            for i, row in enumerate(reader):
                if i == 0:
                    pass
                else:
                    # artist_name = str(row[0])
                    # print(artist_name,  row[1],row[2],"Name@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                    # dob = row[1]
                    # if row[2] =="Female":
                    #     gender = 'f'
                    # elif row[2] =='Male':
                    #     gender = 'm'
                    # elif row[2] =='other':
                    #     gender = 'o'
                    # first_release = row[3]
                    # no_of_album = row[4]
                    try:
                        artist = Artist.objects.create(name=row[0],dob=row[1],gender=row[2],first_release_year=row[3],no_of_albums_released=row[4])
                    except Exception as e:
                        print("##################Error: ",e)
                    
            obj.is_activated = True
            obj.save()
        return redirect('artistapp:a_artist_list')


class CSVExportArtistView(View):
    def get(self, request):
        data = Artist.objects.all()

        # Create the HTTP response with CSV content
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="Artists.csv"'

        # Create a CSV writer and write the data to the response
        writer = csv.writer(response)
        
        # Write header row
        writer.writerow(['Name', 'D.O.B', 'Gender','First Album','Total Album'])

        # Write data rows
        for item in data:
            # if item.gender =='f':
            #     gender ='Female' 
            # elif item.gender =='m':
            #     gender ='Male' 
            # elif item.gender =='o':
            #     gender='Others'
            # item.gender = gender
            # if item.dob:
            #     mydate = str(item.dob)
            #     date_part = datetime.strptime(mydate[:-6], "%Y-%m-%d %H:%M:%S").date()
            #     item.dob = date_part.isoformat()

            writer.writerow([item.name, item.dob,item.gender, item.first_release_year,item.no_of_albums_released])
        
        return response